<?php

/**
 * @file
 * hook_file and imceimage_crop file functions.
 */
function imceimage_crop_file_insert($file) {
  // we copy the file as is, for quality cropping.
  if (imceimage_file_is_image($file)) { 
    $newfile = drupal_clone($file);
    file_copy($newfile->filepath, imceimage_crop_file_admin_crop_display_path($newfile));
  }
}

function imceimage_crop_file_update(&$file) {
  // update admin thumbnail
  imceimage_file_insert($file);

  // flush imagecache
  module_invoke('imagecache', 'image_flush', $file->filepath);

  // update timestamp, see http://drupal.org/node/353405
  $file->timestamp = REQUEST_TIME;
  drupal_write_record('files', $file, 'fid');
}

function imceimage_crop_file_delete($file) {
  // delete admin thumbnail.
  if (imceimage_file_is_image($file))
    file_delete(imceimage_crop_file_admin_crop_display_path($file));
}

// create the path to the file to be used for displaying the crop interface.
function imceimage_crop_file_admin_crop_display_path($file) {
  $file = (object)$file;
  return $file->imceimage_path .'.crop_display.jpg';
}

// Original file to crop from
function imceimage_crop_file_admin_original_path($file) {
  // the displayed and original are the same. Yay Jcrop!
  return imceimage_crop_file_admin_crop_display_path($file);
}
